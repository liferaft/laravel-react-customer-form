# Laravel React Customer Form

A project that uses Laravel as a backend and React JS as a front-end for adding a new customer.

## API

This folder contains Laravel Framework setup to serve customers API.

### Add New Customer API

- URL: http://127.0.0.1:80/api/customers
- Method: POST
- Parameters:

### Add New Customer API

- URL: http://127.0.0.1:80/api/customers
- Method: POST
- Parameters:

```
{
   "name":"xxx",
   "email":"xxx",
   "phone_number":"xxx",
   "address":{
      "house_number":"xxx",
      "street_name":"xxx",
      "city":"xxx",
      "state":"xx",
      "country":"xx"
   }
}
```

- Success Response:

```
{
   "success":true,
   "data":{
      "message":"Record saved successfully!",
      "customer":{
         "name":"xxx",
         "email":"xxx",
         "phone_number":"xxx",
         "address":{
            "house_number":"xxx",
            "street_name":"xxx",
            "city":"xxx",
            "state":"xx",
            "country":"xx"
         }
      }
   }
}
```

- Error Response:

```
   {
   "success":false,
   "errors":{
      "name":[
         "Name must contain at least 2 characters"
      ],
      "address.street_name":[
         "Street name must contain at least 2 characters"
      ],
      "address.city":[
         "The address.city must be at least 2 characters."
      ],
      "address.state":[
         "State must contain at least 2 characters"
      ]
   },
   "message":"Invalid data values"
}
```

## UI

This folder contains ReactJS Framework setup to view add customer Form.

### Accessing Frontend Form

http://localhost:3001/
