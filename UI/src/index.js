import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import CustomerManager from "./components/customer/CustomerManager";
import { CustomerContextProvider } from "./contexts/CustomerContext";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <CustomerContextProvider>
      <CustomerManager />
    </CustomerContextProvider>
  </React.StrictMode>
);
