import React, { createContext } from "react";
import {
  checkEmpty,
  checkNumber,
  checkEmail,
  checkPhone,
  checkAlphaSpace,
} from "../utility/stringFunctions";
import Input from "../components/ui/form/Input";
import UsStates from "../components/ui/form/UsStates";
import CanadianProvince from "../components/ui/form/CanadianProvince";
// Define common context function for managing site customer data
export const CustomerContext = createContext({
  countries: {},
  selectedCountry: {},
  addCustomer: () => {},
  changeSelectedCountry: () => {},
});

// Define the CustomerContext provider component to pass the countries to the child components
export const CustomerContextProvider = (props) => {
  // Initial values are obtained from the props
  const { children } = props;

  // Function to add customer to the website
  const addCustomer = async (customer) => {
    // Add the customer to the website
    const url = "http://localhost:80/api/customers";
    const body = JSON.stringify(customer);
    const method = "POST";
    const mode = "cors";
    const headers = {
      "Content-Type": "application/json",
    };
    const response = await fetch(url, {
      method,
      headers,
      body,
      mode,
    }).then((data) => data.json());

    const success = response?.success ?? false;

    const message = success
      ? "Customer added successfully"
      : "Unable to add Customer";

    const errorsResponse = response?.errors ?? {};

    // Structure Error object
    const errors = {};
    for (const key in errorsResponse) {
      // console.log(key);

      if (key.includes(".")) {
        const keyArray = key.split(".");
        // console.log(keyArray);
        if (keyArray.length === 2) {
          if (!errors[keyArray[0]]) {
            errors[keyArray[0]] = {};
          }
          errors[keyArray[0]][keyArray[1]] = errorsResponse[key][0];
        }
      } else {
        errors[key] = errorsResponse[key][0];
      }
    }

    // Return the response status
    return { success, message, errors };
  };

  const getStateComponent = (country, ref) => {
    if (country === "US") {
      return <UsStates name="state" key="state" id="state" ref={ref} />;
    }
    if (country === "CA") {
      return <CanadianProvince name="state" key="state" id="state" ref={ref} />;
    }
    return <Input type="text" name="state" id="state" ref={ref} />;
  };

  // Validate the form data before adding the customer
  const isValidCustomerData = ({
    customerName,
    email,
    phoneNumber,
    houseNumber,
    streetName,
    city,
    state,
    country,
  }) => {
    //  Create object from form inputs

    let errors = {};
    let addressErrors = {};
    // Check if the name is empty
    if (checkEmpty(customerName)) {
      errors = { ...errors, name: "Name is required" };
    } else if (!checkAlphaSpace(customerName)) {
      errors = { ...errors, name: "Name must contain only letters" };
    }

    if (checkEmpty(email)) {
      errors = { ...errors, email: "Email is required" };
    } else if (!checkEmail(email)) {
      errors = { ...errors, email: "Email is invalid" };
    }

    if (checkEmpty(phoneNumber)) {
      errors = { ...errors, phone_number: "Phone number is required" };
    } else if (!checkPhone(phoneNumber)) {
      errors = { ...errors, phone_number: "Phone number is invalid" };
    }

    if (checkEmpty(houseNumber)) {
      addressErrors = {
        ...addressErrors,
        house_number: "House number is required",
      };
    } else if (!checkNumber(Number(houseNumber))) {
      addressErrors = {
        ...addressErrors,
        house_number: "House number must contain only digits",
      };
    }

    if (checkEmpty(streetName)) {
      addressErrors = {
        ...addressErrors,
        street_name: "Street name is required",
      };
    }

    if (checkEmpty(city)) {
      addressErrors = { ...addressErrors, city: "City is required" };
    } else if (!checkAlphaSpace(city)) {
      errors = { ...addressErrors, city: "City must contain only letters" };
    }

    if (checkEmpty(state)) {
      addressErrors = {
        ...addressErrors,
        state: "State is required",
      };
    } else if (!checkAlphaSpace(state)) {
      errors = { ...addressErrors, state: "State must contain only letters" };
    }

    if (checkEmpty(country)) {
      addressErrors = {
        ...addressErrors,
        country: "Country is required",
      };
    }

    if (Object.keys(addressErrors).length > 0) {
      errors = { ...errors, address: addressErrors };
    }
    const formattedCustomerData = {
      name: customerName,
      email,
      phone_number: phoneNumber,
      address: {
        house_number: houseNumber,
        street_name: streetName,
        city,
        state,
        country,
      },
    };
    return {
      isValid: Object.getOwnPropertyNames(errors).length === 0,
      errors,
      formattedCustomerData,
    };
  };

  // Make the context object:
  const customerContext = {
    addCustomer,
    isValidCustomerData,
    getStateComponent,
  };

  // pass the value in provider and return
  return (
    <CustomerContext.Provider value={customerContext}>
      {children}
    </CustomerContext.Provider>
  );
};

export const { CustomerContextConsumer } = CustomerContext;

CustomerContextProvider.propTypes = {};

CustomerContextProvider.defaultProps = {};
