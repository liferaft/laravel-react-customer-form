import { toast } from "react-toastify";
export const notifySucess = (message) =>
  toast.success(message, { autoClose: 2000 });

export const notifyFailure = (message) =>
  toast.error(message, { autoClose: 2000 });
