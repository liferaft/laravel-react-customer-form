// trimString receives an argument and removed spaces
export const trimString = (str) => (str + "").trim();

// checkNumber arrow function accepts variable length arguments and check whether all are of number type
export const checkNumber = (...args) => {
  return args.every((number) => typeof number == "number");
};
/**
 *
 * @param  {...any} args array of values to be converted to string and test if they are empty
 * @returns {Boolean} true if any values are empty, false otherwise
 */
export const checkEmpty = (...args) =>
  args.some((value) => `${value}`.trim() === "");

// checkEmail receives an argument and validates whether it is a valid e-mail id
export const checkEmail = (...args) =>
  args.every((str) =>
    /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(
      trimString(str)
    )
  );

// checkPhone receives an argument and validates whether it is a valid phone number
export const checkPhone = (...args) =>
  args.every((str) => /^([0-9]{10})$/.test(trimString(str)));

// checkAlphaSpace receives an argument and validates whether it is contains only aphabets and spaces
export const checkAlphaSpace = (...args) =>
  args.every((str) => /^[A-z ]+$/.test(trimString(str)));
