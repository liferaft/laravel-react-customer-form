import React from "react";
import propTypes from "prop-types";
import Input from "./Input";
import Label from "./Label";
// Modularize the stylesheet so that it can be imported in other components and do not clutter the global scope
import classes from "./InputWithLabel.module.css";
// Component to present a form input
const InputWithLabel = React.forwardRef((props, ref) => {
  /**
   *  Use props classes attribute to retrieve css classes from the classes object provided in the component property,
   * otherwise use current css module reference
   *
   */
  return (
    <>
      <Label
        htmlFor="{props.id}"
        className={props.isRequired ? "required" : ""}
      >
        {props.label}
      </Label>
      <Input
        type="{props.type}"
        name="{props.name}"
        id="{props.id}"
        value={props.value}
        ref={ref}
        onChange={props.onChange}
        onBlur={props.onBlur}
        onFocus={props.onFocus}
        min={props.min}
        max={props.max}
        className={`${classes.input} ${
          props.className
            ? props.classes
              ? props.classes[props.className]
              : classes[props.className]
            : ""
        }`}
      />
      {props.error && <span className={classes.error}>{props.error}</span>}
    </>
  );
});
// PropTypes define the types of values that are passed to the components
Input.propTypes = {
  type: propTypes.string,
  label: propTypes.string,
  error: propTypes.string,
  id: propTypes.string,
  name: propTypes.string,
  value: propTypes.string,
  isRequired: propTypes.bool,
  className: propTypes.string,
  onChange: propTypes.func,
  onBlur: propTypes.func,
  onFocus: propTypes.func,
  min: propTypes.number,
  max: propTypes.number,
  classes: propTypes.object,
};
// Default Props of the component
Input.defaultProps = {
  type: "text",
  label: "",
  error: null,
  id: null,
  name: null,
  isRequired: false,
  className: null,
  onChange: null,
  onBlur: null,
  onFocus: null,
  min: null,
  max: null,
  classes: null,
};
// Export the component
export default InputWithLabel;
