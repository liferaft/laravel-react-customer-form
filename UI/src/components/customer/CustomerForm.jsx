import React, { useContext, useRef, useState } from "react";
import propTypes from "prop-types";
import Form from "../ui/form/Form";
import InputWithLabel from "../ui/form/InputWithLabel";
import Button from "../ui/form/Button";
import Row from "../ui/layout/Row";
import Column from "../ui/layout/Column";
import Card from "../ui/layout/Card";
import Label from "../ui/form/Label";
import Country from "../ui/form/Country";
import { CustomerContext } from "../../contexts/CustomerContext";
import { notifyFailure, notifySucess } from "../../utility/notify";
import classes from "./CustomerForm.module.css";

import "react-toastify/dist/ReactToastify.css";

const CustomerForm = (props) => {
  // Variable to track form errors
  const [errors, setErrors] = useState(null);

  // Retrieve the context values (function references)
  const { addCustomer, isValidCustomerData, getStateComponent } =
    useContext(CustomerContext);

  //  References to the form inputs
  const nameRef = useRef("");
  const emailRef = useRef("");
  const phoneNumberRef = useRef("");
  const houseNumberRef = useRef("");
  const streetNameRef = useRef("");
  const cityRef = useRef("");
  const stateRef = useRef("");
  const countryRef = useRef("");

  // Variable to track the state field type (text or select)
  const [stateField, setStateField] = useState(
    getStateComponent(countryRef.current.value, stateRef)
  );

  // Update State Field Type based on the selected country
  const updateStateComponent = () => {
    setStateField(getStateComponent(countryRef.current.value, stateRef));
  };

  // Update the state of the form inputs error messages
  const resetForm = () => {
    // Reset the form inputs
    nameRef.current.value = "";
    emailRef.current.value = "";
    phoneNumberRef.current.value = "";
    houseNumberRef.current.value = "";
    streetNameRef.current.value = "";
    cityRef.current.value = "";
    stateRef.current.value = "";
    countryRef.current.value = "";
    updateStateComponent();
    setErrors(null);
    // Move cursor to the name input
    nameRef.current.focus();
  };

  // Form submission handler
  const submitCustomerHandler = async (event) => {
    // Prevent default form submission
    event.preventDefault();
    const userInput = {
      customerName: nameRef.current.value.trim(),
      email: emailRef.current.value.trim(),
      phoneNumber: phoneNumberRef.current.value.trim(),
      houseNumber: houseNumberRef.current.value.trim(),
      streetName: streetNameRef.current.value.trim(),
      city: cityRef.current.value.trim(),
      state: stateRef.current.value.trim(),
      country: countryRef.current.value.trim(),
    };
    const {
      isValid,
      errors: errorsResult,
      formattedCustomerData,
    } = isValidCustomerData(userInput);
    if (!isValid) {
      setErrors(errorsResult);
      notifyFailure("Please correct the errors in the form");
      return;
    }
    const {
      success,
      message,
      errors: errorsResults,
    } = await addCustomer(formattedCustomerData);
    if (success) {
      // Reset Form
      resetForm();
      notifySucess(message);
    } else {
      // Set the errors
      setErrors(errorsResults);
      notifyFailure(message);
    }
  };
  return (
    <>
      <Card>
        <Form onSubmit={submitCustomerHandler}>
          <Row>
            <Column className="is-one-half">
              <InputWithLabel
                label={props.nameLabel}
                error={errors?.name}
                isRequired={true}
                type="text"
                name="name"
                id="name"
                ref={nameRef}
              />
            </Column>
            <Column className="is-one-half">
              <InputWithLabel
                label={props.emailLabel}
                error={errors?.email}
                isRequired={true}
                type="email"
                name="email"
                id="email"
                ref={emailRef}
              />
            </Column>
          </Row>
          <Row>
            <Column className="is-one-half">
              <InputWithLabel
                label={props.phoneNumberLabel}
                error={errors?.phone_number}
                isRequired={true}
                type="tel"
                name="phone_number"
                id="phone_number"
                ref={phoneNumberRef}
              />
            </Column>

            <Column className="is-one-half">
              <InputWithLabel
                label={props.houseNumberLabel}
                error={errors?.address?.house_number}
                isRequired={true}
                type="text"
                name="house_number"
                id="house_number"
                ref={houseNumberRef}
              />
            </Column>
          </Row>
          <Row>
            <Column className="is-one-half">
              <InputWithLabel
                label={props.streetLabel}
                error={errors?.address?.street_name}
                isRequired={true}
                type="text"
                name="street_name"
                id="street_name"
                ref={streetNameRef}
              />
            </Column>

            <Column className="is-one-half">
              <InputWithLabel
                label={props.cityLabel}
                error={errors?.address?.city}
                isRequired={true}
                type="text"
                name="city"
                id="city"
                ref={cityRef}
              />
            </Column>
          </Row>
          <Row>
            <Column className="is-one-half">
              <Label htmlFor="country" className="required">
                {props.countryLabel}
              </Label>

              <Country
                className={classes.select}
                name="country"
                key="country"
                id="country"
                ref={countryRef}
                onChange={(e) => updateStateComponent()}
              />
              {errors && errors.address && errors.address.country && (
                <span className={classes.error}>{errors.address.country}</span>
              )}
            </Column>
            <Column className="is-one-half">
              <Label htmlFor="state" className="required">
                {props.stateLabel}
              </Label>
              {stateField}
              {errors && errors.address && errors.address.state && (
                <span className={classes.error}>{errors.address.state}</span>
              )}
            </Column>
          </Row>
          <Row>
            <Column className="center">
              <Button type="submit">{props.submitButtonText}</Button>
            </Column>
          </Row>
        </Form>
      </Card>
    </>
  );
};

// PropTypes define the types of values that are passed to the components
CustomerForm.propTypes = {
  nameLabel: propTypes.string,
  emailLabel: propTypes.string,
  phoneNumberLabel: propTypes.string,
  addressLabel: propTypes.string,
  houseNumberLabel: propTypes.string,
  streetLabel: propTypes.string,
  cityLabel: propTypes.string,
  stateLabel: propTypes.string,
  countryLabel: propTypes.string,
  submitButtonText: propTypes.string,
};

// Default Props of the component
CustomerForm.defaultProps = {
  nameLabel: "Name",
  emailLabel: "Email",
  phoneNumberLabel: "Phone Number",
  addressLabel: "Address",
  houseNumberLabel: "House Number",
  streetLabel: "Street Name",
  cityLabel: "City",
  stateLabel: "State/Province",
  countryLabel: "Country",
  submitButtonText: "Save",
};

// Export the component

export default CustomerForm;
