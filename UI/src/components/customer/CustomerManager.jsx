import React from "react";
import propTypes from "prop-types";
import classes from "./CustomerManager.module.css";
import CustomerForm from "./CustomerForm";
import { ToastContainer } from "react-toastify";
//  Holds data for customer Page and handles the logic for the customer page
const CustomerManager = (props) => {
  return (
    <div className={classes.container}>
      <h1 className={classes.title}>{props.title}</h1>
      <CustomerForm />
      <ToastContainer newestOnTop={true} />
    </div>
  );
};

// PropTypes define the types of values that are passed to the components
CustomerManager.propTypes = {
  title: propTypes.string,
};

// Default Props of the component
CustomerManager.defaultProps = {
  title: "Add New Customer",
};

// Export the component
export default CustomerManager;
