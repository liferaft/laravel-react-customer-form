<?php 

namespace App\Traits;

// StorageFileHandler trait to handle the storage file.
trait StorageFileHandler{

    // Method to get the path of data directory in the storage.
    public static function getDataDirectory() : string {
        return storage_path('app'.DIRECTORY_SEPARATOR.'data');
    }

    // Method to check if the file exists.
    public static function isExistingDataFile($fileName) : bool {
        return file_exists(self::getDataDirectory().DIRECTORY_SEPARATOR.$fileName); 
    }

    // Method to get full path of file.
    public static function getFilePath($fileName) : string {
        return self::getDataDirectory().DIRECTORY_SEPARATOR.$fileName; 
    }

    // Method to write the data from the storage file.
    public static function createFile($fileName) : bool {
        if(self::isExistingDataFile($fileName)):
            return true;
        endif;
        $file = fopen(self::getDataDirectory().DIRECTORY_SEPARATOR.$fileName, "w");
        fclose($file);
        return true;
    }

    // Method to write json data to the storage file.
    public static function writeJSON(string $fileName, object ...$data) : bool {
        $filePath = self::getFilePath($fileName);    
        $fh = fopen($filePath, 'w');
        fwrite($fh, json_encode($data));
        fclose($fh);
        return true;        
    }

    // Method to read the data from a file.
    public static function read(string $fileName) : string{
        $filePath = self::getFilePath($fileName);
        $fh = fopen($filePath, 'r');
        $fileSize = filesize($filePath);
        if ($fileSize > 0) :
            $fileData = fread($fh, $fileSize);
        else :
            $fileData = "";
        endif;            
        fclose($fh);
        return $fileData;
    }

    // read JSON data from the storage file.
    public static function readJson(string $fileName) : array{
        $data = self::read($fileName);
        if ($data == "") :
            return [];
        endif;
        return json_decode($data);
    }
}