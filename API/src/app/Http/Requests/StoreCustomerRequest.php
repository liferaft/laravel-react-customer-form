<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public  function messages($id = '') {
    return [
        'name.required' => 'Name is required',
        'email.required' => 'Email is required',
        'phone_number.required' => 'Phone number is required',
        'address.required' => 'Address details are required',
        'address.house_number.required' => 'House number is required',
        'address.street_name.required' => 'Street name is required',
        'address.city.required' => 'City is required',
        'address.country.required' => 'Country is required',
        'name.min' => 'Name must contain at least 2 characters',
        'phone_number.digits' => 'Phone number must contain 10 digits',
        'phone_number.numeric' => 'Phone number must contain 10 digits',
        'address.house_number.numeric' => 'House number must contain only numbers',
        'address.street_name.min' => 'Street name must contain at least 2 characters',
        'address.street_name.max' => 'Street name must contain at max 100 characters',
        'address.state.min' => 'State must contain at least 2 characters',
        'address.state.max' => 'State must contain at max 100 characters',
        'address.country.min' => 'Country must contain 2 characters',
        'address.country.max' => 'Country must contain 2 characters',
    ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
                'name' => 'required|string|min:2|max:100',
                'email' => 'required|string|email',
                'phone_number' => 'required|numeric|digits:10',
                'address' => 'required|array',
                'address.house_number' => 'required|numeric|min:1',
                'address.street_name' => 'required|string|min:2|max:100',
                'address.city' => 'required|string|min:2|max:100',
                'address.state' => 'required|string|min:2|max:100',
                'address.country' => 'required|string|min:2|max:2',
        ];
    }
}