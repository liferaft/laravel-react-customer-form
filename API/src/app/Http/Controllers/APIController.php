<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\APIResponser;

// APIController as a base class for all the controllers that handles API requests.
class APIController extends Controller
{
    // Use the APIResponser trait to handle the API responses.
    use APIResponser;
}